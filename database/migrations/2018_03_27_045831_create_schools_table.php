<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schools', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_code');
            $table->string('school_name');
            $table->string('school_rep');
            $table->string('logo')->default('default.jpg');
            $table->string('email')->unique();
            $table->string('telephone')->unique();
            $table->string('address');
            $table->integer('subscribe');
            $table->string('sub_type');
            $table->integer('no_students')->default('0');
            $table->string('storage')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schools');
    }
}
